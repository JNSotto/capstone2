const express = require('express');
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

//User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//User Authentication
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Authenticated admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
	userController.promoteAdmin(req.params).then(resultFromController=> res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
})

//User Order Item (NON-ADMIN ONLY)
router.post("/checkOut",auth.verify,(req, res) =>{
	const userData = auth.decode(req.headers.authorization)

	userController.orderItem(userData,req.body).then(resultFromController => 
		res.send(resultFromController))

})

//Retrieve authenticated user's orders
router.get("/:userId/myOrder",auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization)

	
	userController.getOrder(userData,req.params).then(resultFromController=> res.send(resultFromController));
	
})

//Retrieve all Orders (ADMIN ONLY)
router.get("/orders", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	}else{
		res.send('Not Authorized')
	}
})

module.exports = router;
