const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const OrderItem = require("../models/OrderItem")
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		name: reqBody.name,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		address: reqBody.address,
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return ('You Successfully Registered')
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return ('The Email is Invalid, Please Enter Correct Email Address! ')
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			}
			else {
				return ('The Password you’ve Entered is Incorrect, Please Try Again! ')
			}
		}
	})
}

//Authenticated Admin
module.exports.promoteAdmin = (reqParams) => {

	return User.findById(reqParams.userId).then(result=> {
		if(result.isAdmin === false){

			result.isAdmin = true
			return result.save().then((promote,err) =>{
				if(err){
					return false
				}else{
					return ('User Promoted to Admin')

				}
			})
		}else{
			return ('The User is Already Admin')
		}


	})
}

//Order CheckOut
module.exports.orderItem = async(userData,reqBody) =>{
	 
	//user authentication, only user who login will get access
	if(userData.isAdmin === false && userData.id === reqBody.customer){	
		//This is for input product and quantity in a orderItem model to send the data to the orderList 
		//I put Promise.all() to forcely fulfill the promises in the function 
		const orderItemsIds = Promise.all(reqBody.orderList.map(async orderItem =>{
			let newOrderItem = new OrderItem({
				product:orderItem.product,
				quantity:orderItem.quantity
			})
			newOrderItem = await newOrderItem.save();

			return newOrderItem._id;
		}))

	//this variable is for fulfilling the last promise of orderItemsIds
	const orderItemsIdsfixed = await orderItemsIds;


		//This is for the computing the TotalAmount of the OrderItems 
		const totalPrices = await Promise.all(orderItemsIdsfixed.map(async (orderItemId) =>{
			const orderItem = await OrderItem.findById(orderItemId).populate('product','price')
			const totalPrice = orderItem.product.price * orderItem.quantity;
			return totalPrice
		}))

		//the Array.prototype.reduce() method, it reduce the elements into a single value (a,b), (previousValue + currentValue,0) - it sum up all the elements in an array
		const totalPrice = totalPrices.reduce((previousValue, currentValue)=> previousValue + currentValue)

		//This is for deducting the Stocks of the product, when the user order a product with quantity, quantity is subtacted on the inStock Object of the Product.
		const numStocks = await Promise.all(orderItemsIdsfixed.map(async (orderItemId) =>{
			const orderItem = await OrderItem.findById(orderItemId).populate('product','inStock')
			const numStock = orderItem.product.inStock - orderItem.quantity;
			return await Product.findById(orderItem.product).then(result=> {
				result.inStock = numStock
				return result.save()
			})	
			
		}))

		// This is for automatically Archiving the product if the inStock turns to less than zero. 
		const isActiveUpdate = await Promise.all(orderItemsIdsfixed.map(async (orderItemId) =>{
			const orderItem = await OrderItem.findById(orderItemId).populate('product','inStock')
			return await Product.findById(orderItem.product).then(result=> {
				if(result.inStock <=0){
					result.isActive = false
					return result.save()
				}
			})	
			
		}))

		//This is the order checkOut which is going to be displayed to the user
		let order = await new Order ({
			customer:reqBody.customer,
			orderList: orderItemsIdsfixed,
			totalAmount: totalPrice
		})

		return await order.save().then((orderCheckedOut, error)=>{
			if(error){
				return false
			} else {
				return orderCheckedOut
			}
		})

	} else {
		return ('Only User Owner Can Purchased Product')
	}
}


//Retrieve authenticated user's orders
module.exports.getOrder = async(userData,reqParams) =>{

	if(userData.isAdmin === false && userData.id === reqParams.userId){
		return Order.find({customer:reqParams.userId}).populate('customer').populate({path:'orderList',populate:'product'}).sort({'purchasedOn': -1}).then(result=> {
			if(result.length === 0){
				return ('Order is Empty')
			} else {
				return result
			}
		
		})

	} else {
		return ('Only User Owner Can View Their Orders')
	}

}

//Retrieve all Orders (ADMIN ONLY)
module.exports.getAllOrders = () => {
	return Order.find().populate('customer').populate({path:'orderList',populate:'product'}).sort({'purchasedOn': -1}).then(result =>{
		return result;
	})
}

