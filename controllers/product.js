const Product = require("../models/Product");

//Create a Product (ADMIN ONLY)
module.exports.addProduct = (reqBody) => {
	
	let newProduct = new Product({
		category:reqBody.category,
		name:reqBody.name,
		brand:reqBody.brand,
		description: reqBody.description,
		price:reqBody.price,
		inStock:reqBody.inStock

	})

	return newProduct.save().then((product,err) =>{
		if(err) {
			return false
		} else {
			return ('Product is Succussfully Added')
		}
	})
} 

//Retrieve all active product
module.exports.getAllActiveProduct = () =>{
	return Product.find({isActive:true}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve All Active Products According to Categories
module.exports.getAllActiveProductCategory = (reqParams) =>{
	return Product.find({isActive:true,category:reqParams.category}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve All Active Products According to Brands
module.exports.getAllActiveProductBrand = (reqParams) =>{
	return Product.find({isActive:true,brand:reqParams.brand}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve All Active Products According to Categories and Brands
module.exports.getAllActiveProductCategoryBrand = (reqParams) =>{
	return Product.find({isActive:true,category:reqParams.category,brand:reqParams.brand}).sort({'createdOn': -1}).then(result =>{
		return result;
	})
}

//Retrieve specific product
module.exports.getProduct = (reqParams) =>{

	return Product.findById(reqParams.productId).then(result=> {
		if(result.isActive === true){
			let product = {
				category:result.category,
				name:result.name,
				brand:result.brand,
				price:result.price
			}
			return product;

		}else {
			return (`The product ${result.name} is Out-Of-Stocks`)
		}
	})
}

//Update Product(ADMIN ONLY)
module.exports.updateProduct = async(userData,reqParams,reqBody) =>{
	if(userData.isAdmin === true){
		let updatedProduct = {
			category:reqBody.category,
			brand:reqBody.brand,
			description: reqBody.description,
			name:reqBody.name,
			price:reqBody.price,
			inStock:reqBody.inStock
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,err)=>{
			if(err){
				return false
			} else {
				return ('Product Information is Succussfully Updated ')
			}
		})
	} else {
		return ('Not Authorized')
	}
}


//Archiving a Product (ADMIN ONLY)

module.exports.archiveProduct = (reqParams) =>{

	return Product.findById(reqParams.productId).then(result=> {
		if(result.isActive === true){

			result.isActive = false
			return result.save().then((archived,err) =>{
				if(err){
					return false
				} else{
					return ('Product is Succussfully Archived')

				}
			})

		}else {
			return ('The Product is Already Archived')
		}


	})
}