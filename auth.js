 const jwt = require("jsonwebtoken");
const secret = 'EcommerceAPI';

module.exports.createAccessToken = (user) => {
	//The data will be received from the registration form 
	//When the user logs in, a token will be created with user's information 
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}


	//Generate a JSON web token using the jwt's sign method
	//Generates the token the form data, and the secret code with no additional options provided
	return jwt.sign(data, secret,{})
}

module.exports.verify = (req, res, next) => {
	//The token is retrieved from the request header 
	let token = req.headers.authorization;

	//Token received and not undefined
	if(typeof token !== "undefined"){
		console.log(token)

		//The token sent is a type of "bearer" token which received contains the "Bearer" as prefix to the string 
			//Syntax: string.slice(start, end)
		//slice and start from 7, to not include the "Bearer" in the token
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return res.send({auth: "failed"})
			} else {

				next()
			}
		})
	}  else {
		return res.send({auth:"failed"})
	}
}

module.exports.decode = (token) => {
	
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			if(err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} else {
		return null
	}
}