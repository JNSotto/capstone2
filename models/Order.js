const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({

	customer:{
		type:mongoose.Schema.Types.ObjectId,
		ref:'User',
		required:true
	},
	orderList:[{

		type:mongoose.Schema.Types.ObjectId,
		ref:'OrderItem',
		require:true
		
	}],

	totalAmount:{
		type:Number,
		default:0
	},
	purchasedOn:{
		type:Date,
		default: Date.now
	}

});

module.exports = mongoose.model('Order', orderSchema)