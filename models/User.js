const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	name:{
		type: String,
		required: [true, 'Please Enter Your FirstName']
	},

	email:{
		type: String,
		required: [true, 'Please Enter Your Email']
	},

	password:{
		type: String,
		required: [true, 'Please Enter Your Password']
	},

	address:{
		type: String,
		required: [true, 'Please Enter Your Complete Address']
	},
	
	mobileNo:{
		type: String,
		required: [true, 'Please Enter Your Mobile Number']
	},

	isAdmin:{
		type:Boolean,
		default: false
	}
});

module.exports = mongoose.model('User', userSchema)