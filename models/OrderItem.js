const mongoose = require('mongoose')

const orderItemSchema = new mongoose.Schema({


	product:{
		type:mongoose.Schema.Types.ObjectId,
		ref:'Product',
		require:true
	},
	quantity:{
		type:Number,
		default:1
	}
		
});

module.exports = mongoose.model('OrderItem', orderItemSchema)